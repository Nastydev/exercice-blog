<?php
session_start();
require '../database/connexon.php';

$error =  [];

if (isset($_POST)){

    if (empty($_POST['title'])){
        $error[] = "Titre obligatoire";
    }

    if (empty($_POST['contente'])){
        $error[] = "Contenue de l'article obligatoire";
    }

    if (!empty($error)){
        $_SESSION['error'] = $error;
        header("location: index.php");
        exit();
    } else {
        function verification($donnees)
        {
            $donnees = trim($donnees);
            $donnees = stripslashes($donnees);
            $donnees = htmlspecialchars($donnees);
            $donnees = preg_replace("/\s+/", " ", $donnees);
            return $donnees;
        }

        $contente = verification($_POST['contente']);
        $title = verification($_POST['title']);
        $date = date("Y-m-d");

        $reqNewArticle = $db->prepare("INSERT INTO article
    (category_id, user_id, title, contente, created_date) VALUES (:category_id, :user_id, :title, :contente, :created_date) ");
        $reqNewArticle -> execute([
            "category_id" => 1 ,
            "user_id" => 1,
            "title"  => $title,
            "contente" => $contente,
            "created_date" => $date,
        ]);

        header("location: index.php");
        exit();
    }
}