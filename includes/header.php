<?php

use Twig\Environment;
use Twig\Loader\FilesystemLoader;

// récupération des paquets composer
require_once 'vendor/autoload.php';

// configuration de twig
$loader = new FilesystemLoader('templates');
$twig = new Environment($loader, [
    'cache' => false,
]);

// connexion à la base de données
$db = '../database/connexon.php';
