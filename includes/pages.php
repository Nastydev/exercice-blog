<?php

class Pages
{

    // page liste des ingrédients
    function index()
    {
        global $db, $twig;

        // rendu de la page liste
        echo $twig->render('index.html.twig');
    }

}
