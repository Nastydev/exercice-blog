<?php
require 'includes/header.php';
?>
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Tableau de bord</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/admin/">Accueil</a></li>
                        <li class="breadcrumb-item active">Tableau de bord</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Articles</h3>
                        </div>
                        <div>
                            <?php if (isset( $_SESSION['error'])){
                                echo implode("<br>", $_SESSION['error']);
                            } ?>
                        </div>
                        <form action="new-article.php" method="post">
                            <div>
                                <label for="title">Titre</label>
                                <input type="text" name="title" id="title">
                            </div>
                            <div>
                                <textarea name="contente" id="contente" cols="30" rows="10"></textarea>
                            </div>
                            <div>
                                <input type="submit">
                            </div>
                        </form>

                    </div>
                    <!-- /.col-md-6 -->

                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content -->
<?php
require 'includes/footer.php';
